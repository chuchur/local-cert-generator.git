在本地生成用于 localhost 和 127.0.0.1 的 SSL 证书，主要包括创建自签名证书和将其配置到服务器。以下是详细步骤：

1. 生成自签名证书

(1) 生成私钥和证书签名请求 (CSR)

openssl req -new -newkey rsa:2048 -nodes -keyout localhost.key -out localhost.csr

在提示中输入信息：
	•	Common Name (CN): 输入 localhost 或 127.0.0.1。
	•	其他字段可以留空或填写虚拟信息。

(2) 创建自签名证书

openssl x509 -req -days 365 -in localhost.csr -signkey localhost.key -out localhost.crt

这将生成一个有效期为 365 天的自签名证书 localhost.crt。

2. 为本地开发环境设置 SAN (Subject Alternative Name)

如果需要支持 localhost 和 127.0.0.1，可以通过配置 SAN 扩展。

(1) 创建 OpenSSL 配置文件

创建 openssl.cnf 文件，并添加以下内容：

[ req ]
default_bits       = 2048
default_keyfile    = localhost.key
distinguished_name = req_distinguished_name
req_extensions     = req_ext
x509_extensions    = v3_ca

[ req_distinguished_name ]
countryName         = Country Name (2 letter code)
countryName_default = US
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = California
localityName                = Locality Name (eg, city)
localityName_default        = San Francisco
organizationName            = Organization Name (eg, company)
organizationName_default    = MyCompany
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_default          = localhost

[ req_ext ]
subjectAltName = @alt_names

[ v3_ca ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = localhost
IP.1 = 127.0.0.1

(2) 使用配置文件生成证书

openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
  -keyout localhost.key \
  -out localhost.crt \
  -config openssl.cnf

3. 将证书添加到信任列表

macOS

将证书导入系统钥匙串，并将其设置为始终信任：

sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain localhost.crt

Windows
	•	双击生成的 localhost.crt 文件。
	•	选择 “安装证书” > “本地计算机” > “受信任的根证书颁发机构”。

Linux

将证书复制到系统的信任目录（例如 /usr/local/share/ca-certificates），然后更新证书存储：

sudo cp localhost.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates

4. 配置服务器

在服务器（如 Nginx、Apache 或 Node.js）中使用生成的 localhost.key 和 localhost.crt：

Nginx 配置示例：

server {
    listen 443 ssl;
    server_name localhost;

    ssl_certificate     /path/to/localhost.crt;
    ssl_certificate_key /path/to/localhost.key;

    location / {
        root /var/www/html;
        index index.html;
    }
}

Node.js 示例：

const https = require('https');
const fs = require('fs');

const options = {
    key: fs.readFileSync('/path/to/localhost.key'),
    cert: fs.readFileSync('/path/to/localhost.crt'),
};

https.createServer(options, (req, res) => {
    res.writeHead(200);
    res.end('Hello, HTTPS!');
}).listen(443);

完成后，通过 https://localhost 或 https://127.0.0.1 测试。若遇到浏览器警告，需手动信任证书。